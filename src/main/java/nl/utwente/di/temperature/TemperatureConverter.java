package nl.utwente.di.temperature;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class TemperatureConverter extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Set response content type
        response.setContentType("text/html");

        // Get the Celsius temperature parameter from the request
        String celsiusParam = request.getParameter("celsius");

        // Convert the Celsius temperature to Fahrenheit
        double celsius = Double.parseDouble(celsiusParam);
        double fahrenheit = celsius * 1.8 + 32;

        // Output the result
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Temperature Converter Result</title></head><body>");
        out.println("<h1>Temperature Converter Result</h1>");
        out.println("<p>" + celsius + " degrees Celsius = " + fahrenheit + " degrees Fahrenheit.</p>");
        out.println("</body></html>");
    }
}
